import React, { Component } from 'react'
import './App.css'
import EmailForm from './EmailForm'
import MessageForm from './MessageForm'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const addChannel = gql`
mutation addChannel ($id: ID!) {
  addChannel(id: $id) {
    id
  }
}
`

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isEmailSubmitted: false,
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  onSubmit (email) {
    this.setState({
      isEmailSubmitted: true,
      email,
    })
    this.props.mutate({
      variables: { id: email },
    })
  }

  handleChange (event) {
    this.setState({
      message: event.target.value,
    })
  }

  render () {
    if (!this.state.isEmailSubmitted) {
      return (
        <EmailForm onSubmit={this.onSubmit} />
      )
    } else {
      return (
        <MessageForm channelId={this.state.email} />
      )
    }
  }
}

export default graphql(addChannel)(App)
