import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'


let subscription = gql`
subscription getNewMessages($channelId: ID!) {
  messageAdded(channelId: $channelId) {
    text
    id
  }
}
`

let Query = gql`
query getMessages($id: ID!) {
  channel(id: $id) {
    messages {
      text
      id
    }
  }
} 
`

class SingleMessage extends Component {
  render () {
    return <p style={{ width: '60%', borderWidth: '1px', borderStyle: 'solid', borderRadius: '5px', }} >{this.props.message}</p>
  }
}

class Messages extends Component {
  componentWillMount () {
    this.props.data.subscribeToMore({
      document: subscription,
      variables: {
        channelId: this.props.channelId
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) {
          return prev
        }

        const newMessage = subscriptionData.data.messageAdded
        return {
          ...prev,
          channel: {
            ...prev.channel,
            messages: [...prev.channel.messages, newMessage],
          }
        }
      }
    })
  }

  render () {
    if (this.props.data.channel) {
      return (
        <div>
          {this.props.data.channel.messages.map((message) => <SingleMessage key={`${message.id}.${message.text}`} message={message.text} />)}
        </div>
      )
    } else {
      return null
    }
  }
}

const MessagesWithData = graphql(Query, {
  options: (props) => ({ variables: { id: props.channelId } }),
})(Messages)

export default MessagesWithData
