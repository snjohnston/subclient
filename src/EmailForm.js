import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class EmailForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
    }
    this.onEmailChange = this.onEmailChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onEmailChange (event) {
    this.setState({
      email: event.target.value,
    })
  }

  onSubmit () {
    this.props.onSubmit(this.state.email)
  }

  render () {
    return (
      <div className="App">
        <input value={this.state.email} onChange={this.onEmailChange} />
        <button onClick={this.onSubmit} >
          Activate Lasers
        </button>
      </div>
    );
  }
}

export default EmailForm;
